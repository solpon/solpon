import os
import cv2

from PyQt5 import QtCore, QtGui, QtWidgets

class MainWindow(QtWidgets.QMainWindow):
    InitialXShift = 51
    InitialYShift = 32
    InitialZShift = 95
    InitialFocalLength = 380
    InitialXAngle = 0
    InitialYAngle = 278
    InitialZAngle = 0    
    
    def __init__(self):
        super().__init__()
        LiveViewGroupBox = self.CreateLiveViewGroupBox()
        ModelCalibrationGroupBox = self.CreateModelCalibrationGroupBox()
        DeterminigMotionAreaGroupBox = self.CreateDeterminigMotionAreaGroupBox()
        MotionDetectionGroupBox = self.CreateMotionDetectionGroupBox()
        MeasurementGroupBox = self.CreateMeasurementGroupBox()
        CentralWidget = QtWidgets.QWidget()
        CentralWidget.setObjectName("CentralWidget")
        LiveViewGroupBox.setParent(CentralWidget)
        ModelCalibrationGroupBox.setParent(CentralWidget)
        DeterminigMotionAreaGroupBox.setParent(CentralWidget)
        MotionDetectionGroupBox.setParent(CentralWidget)
        MeasurementGroupBox.setParent(CentralWidget)
        SizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        SizePolicy.setHorizontalStretch(0)
        SizePolicy.setVerticalStretch(0)
        SizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setFixedSize(1280, 620)
        self.setObjectName("MainWindow")
        self.setCentralWidget(CentralWidget)
        self.setSizePolicy(SizePolicy)
        self.setWindowTitle("System Ochrony Ludności Przed Ogromnym Niebezpieczeństwem")
        self.GoLightImage = QtGui.QPixmap(os.path.join("Resources", "Go.png"))
        self.InactiveGoLightImage = QtGui.QPixmap(os.path.join("Resources", "GoInactive.png"))
        self.StopLightImage = QtGui.QPixmap(os.path.join("Resources", "Stop.png"))
        self.InactiveStopLightImage = QtGui.QPixmap(os.path.join("Resources", "StopInactive.png"))         
        self.TurnOnLigths()
        
    def CreateLiveViewGroupBox(self):
        LiveViewGroupBox = QtWidgets.QGroupBox()
        LiveViewGroupBox.setGeometry(QtCore.QRect(20, 10, 471, 291))
        LiveViewGroupBox.setTitle("Live view")
        LiveViewGroupBox.setObjectName("")
        LiveViewLabel = QtWidgets.QLabel(LiveViewGroupBox)
        LiveViewLabel.setGeometry(QtCore.QRect(10, 20, 451, 261))
        LiveViewLabel.setText("")
        LiveViewLabel.setObjectName("LiveViewLabel")
        self._SetLiveViewImage = LiveViewLabel.setPixmap
        return LiveViewGroupBox
    
    def CreateModelCalibrationGroupBox(self):
        ModelCalibrationGroupBox = QtWidgets.QGroupBox()
        ModelCalibrationGroupBox.setGeometry(QtCore.QRect(500, 10, 761, 291))
        ModelCalibrationGroupBox.setObjectName("ModelCalibrationGroupBox")
        ModelCalibrationGroupBox.setTitle("Model calibration")
        ModelCalibrationLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        ModelCalibrationLabel.setGeometry(QtCore.QRect(10, 20, 451, 261))
        ModelCalibrationLabel.setText("")
        ModelCalibrationLabel.setObjectName("ModelCalibrationLabel")
        XAxisSlider = QtWidgets.QSlider(ModelCalibrationGroupBox)
        XAxisSlider.setGeometry(QtCore.QRect(490, 40, 251, 19))
        XAxisSlider.setOrientation(QtCore.Qt.Horizontal)
        XAxisSlider.setObjectName("XAxisSlider")
        XAxisSlider.setMinimum(-150)
        XAxisSlider.setMaximum(150)
        XAxisSlider.setValue(self.InitialXShift)
        XAxisSlider.valueChanged.connect(self.XShiftChanged)
        YAxisSlider = QtWidgets.QSlider(ModelCalibrationGroupBox)
        YAxisSlider.setGeometry(QtCore.QRect(490, 90, 251, 19))
        YAxisSlider.setOrientation(QtCore.Qt.Horizontal)
        YAxisSlider.setObjectName("YAxisSlider")
        YAxisSlider.setMinimum(-150)
        YAxisSlider.setMaximum(150)
        YAxisSlider.setValue(self.InitialYShift)        
        YAxisSlider.valueChanged.connect(self.YShiftChanged)
        ZAxisSlider = QtWidgets.QSlider(ModelCalibrationGroupBox)
        ZAxisSlider.setGeometry(QtCore.QRect(490, 140, 251, 19))
        ZAxisSlider.setOrientation(QtCore.Qt.Horizontal)
        ZAxisSlider.setObjectName("ZAxisSlider")
        ZAxisSlider.setMinimum(-150)
        ZAxisSlider.setMaximum(150)
        ZAxisSlider.setValue(self.InitialZShift)  
        ZAxisSlider.valueChanged.connect(self.ZShiftChanged)
        FocalLengthSlider = QtWidgets.QSlider(ModelCalibrationGroupBox)
        FocalLengthSlider.setGeometry(QtCore.QRect(490, 190, 251, 19))
        FocalLengthSlider.setOrientation(QtCore.Qt.Horizontal)
        FocalLengthSlider.setObjectName("FocalLengthSlider")
        FocalLengthSlider.setMinimum(100)
        FocalLengthSlider.setMaximum(500)
        FocalLengthSlider.setValue(self.InitialFocalLength)          
        FocalLengthSlider.valueChanged.connect(self.FocalLengthChanged)
        XAxisSliderLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        XAxisSliderLabel.setGeometry(QtCore.QRect(490, 20, 47, 13))
        XAxisSliderLabel.setObjectName("XAxisSliderLabel")
        XAxisSliderLabel.setText("X Axis")
        YAxisSliderLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        YAxisSliderLabel.setGeometry(QtCore.QRect(490, 70, 47, 13))
        YAxisSliderLabel.setObjectName("YAxisSliderLabel")
        YAxisSliderLabel.setText("Y Axis")
        ZAxisSliderLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        ZAxisSliderLabel.setGeometry(QtCore.QRect(490, 120, 47, 13))
        ZAxisSliderLabel.setObjectName("ZAxisSliderLabel")
        ZAxisSliderLabel.setText("Z Axis")
        FocalLengthSliderLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        FocalLengthSliderLabel.setGeometry(QtCore.QRect(490, 170, 61, 16))
        FocalLengthSliderLabel.setObjectName("FocalLengthSliderLabel")
        FocalLengthSliderLabel.setText("Focal length")
        XRotationDial = QtWidgets.QDial(ModelCalibrationGroupBox)
        XRotationDial.setGeometry(QtCore.QRect(490, 230, 50, 51))
        XRotationDial.setObjectName("XRotationDial")
        XRotationDial.setMaximum(359)
        XRotationDial.setValue(self.InitialXAngle)
        XRotationDial.valueChanged.connect(self.XAngleChanged)
        YRotationDial = QtWidgets.QDial(ModelCalibrationGroupBox)
        YRotationDial.setGeometry(QtCore.QRect(590, 230, 50, 51))
        YRotationDial.setObjectName("YRotationDial")
        YRotationDial.setMaximum(359)
        YRotationDial.setValue(self.InitialYAngle)
        YRotationDial.valueChanged.connect(self.YAngleChanged)
        ZRotationDial = QtWidgets.QDial(ModelCalibrationGroupBox)
        ZRotationDial.setGeometry(QtCore.QRect(690, 230, 50, 51))
        ZRotationDial.setObjectName("ZRotationDial")
        ZRotationDial.setMaximum(359)
        ZRotationDial.setValue(self.InitialZAngle)
        ZRotationDial.valueChanged.connect(self.ZAngleChanged)
        XRotationDialLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        XRotationDialLabel.setGeometry(QtCore.QRect(490, 220, 47, 13))
        XRotationDialLabel.setObjectName("XRotationDialLabel")
        XRotationDialLabel.setText("X rotation")
        YRotationDialLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        YRotationDialLabel.setGeometry(QtCore.QRect(590, 220, 47, 13))
        YRotationDialLabel.setObjectName("YRotationDialLabel")
        YRotationDialLabel.setText("Y rotation")
        ZRotationDialLabel = QtWidgets.QLabel(ModelCalibrationGroupBox)
        ZRotationDialLabel.setGeometry(QtCore.QRect(690, 220, 47, 13))
        ZRotationDialLabel.setObjectName("ZRotationDialLabel")        
        ZRotationDialLabel.setText("Z rotation")
        self._SetModelCalibrationImage = ModelCalibrationLabel.setPixmap
        return ModelCalibrationGroupBox
    
    def CreateDeterminigMotionAreaGroupBox(self):
        DeterminigMotionAreaGroupBox = QtWidgets.QGroupBox()
        DeterminigMotionAreaGroupBox.setGeometry(QtCore.QRect(20, 310, 471, 291))
        DeterminigMotionAreaGroupBox.setObjectName("DeterminigMotionAreaGroupBox")
        DeterminigMotionAreaGroupBox.setTitle("Determinig motion area")
        DeterminigMotionAreaLabel = QtWidgets.QLabel(DeterminigMotionAreaGroupBox)
        DeterminigMotionAreaLabel.setGeometry(QtCore.QRect(10, 20, 451, 261))
        DeterminigMotionAreaLabel.setText("")
        DeterminigMotionAreaLabel.setObjectName("DeterminigMotionAreaLabel")
        self._SetDeterminigMotionAreaImage = DeterminigMotionAreaLabel.setPixmap
        return DeterminigMotionAreaGroupBox
    
    def CreateMotionDetectionGroupBox(self):
        MotionDetectionGroupBox = QtWidgets.QGroupBox()
        MotionDetectionGroupBox.setGeometry(QtCore.QRect(500, 310, 471, 291))
        MotionDetectionGroupBox.setObjectName("MotionDetectionGroupBox")
        MotionDetectionGroupBox.setTitle("Motion detection")
        MotionDetectionLabel = QtWidgets.QLabel(MotionDetectionGroupBox)
        MotionDetectionLabel.setGeometry(QtCore.QRect(10, 20, 451, 261))
        MotionDetectionLabel.setText("")
        MotionDetectionLabel.setObjectName("MotionDetectionLabel")
        self._SetMotionDetectionLabelImage = MotionDetectionLabel.setPixmap
        return MotionDetectionGroupBox
    
    def CreateMeasurementGroupBox(self):
        MeasurementGroupBox = QtWidgets.QGroupBox()
        MeasurementGroupBox.setGeometry(QtCore.QRect(990, 310, 271, 291))
        MeasurementGroupBox.setObjectName("MeasurementGroupBox")
        MeasurementGroupBox.setTitle("Measurement and alarming")
        GoLightLabel = QtWidgets.QLabel(MeasurementGroupBox)
        GoLightLabel.setGeometry(QtCore.QRect(20, 40, 111, 91))
        GoLightLabel.setObjectName("GoLightLabel")       
        StopLightLabel = QtWidgets.QLabel(MeasurementGroupBox)
        StopLightLabel.setGeometry(QtCore.QRect(140, 40, 111, 91))
        StopLightLabel.setObjectName("StopLightLabel")
        InfoLabel = QtWidgets.QLabel(MeasurementGroupBox)
        InfoLabel.setGeometry(QtCore.QRect(20, 140, 231, 91))
        InfoLabel.setObjectName("InfoLabel")
        InfoLabel.setText("")
        InfoLabel.setWordWrap(True)
        self.SetGoLigthImage = GoLightLabel.setPixmap
        self.SetStopLigthImage = StopLightLabel.setPixmap
        self.SetInfoText = InfoLabel.setText
        return MeasurementGroupBox
    
    def TurnOnLigths(self):
        self.SetGoLigthImage(self.GoLightImage)
        self.SetStopLigthImage(self.InactiveStopLightImage)
    
    def TurnOffLigths(self):
        self.SetGoLigthImage(self.InactiveGoLightImage)
        self.SetStopLigthImage(self.StopLightImage)    
    
    def XShiftChanged(self, Value):
        pass
    
    def YShiftChanged(self, Value):
        pass
    
    def ZShiftChanged(self, Value):
        pass    
    
    def FocalLengthChanged(self, Value):
        pass
    
    def XAngleChanged(self, Value):
        pass    
    
    def YAngleChanged(self, Value):
        pass    

    def ZAngleChanged(self, Value):
        pass

    def ExecuteOnGUIThread(self, Function):
        QtCore.QTimer.singleShot(0, Function)
    
    @staticmethod
    def GetPanelImage(Data):
        PixmapObject = QtGui.QPixmap()
        PixmapObject.loadFromData(cv2.imencode(".bmp", Data)[1])
        return PixmapObject.scaled(451, 261, QtCore.Qt.KeepAspectRatioByExpanding)
