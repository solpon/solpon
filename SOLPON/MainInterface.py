import os

import MainWindow
import Mapping3D
import PhotoDivider
import VideoProvider

class MainInterface(MainWindow.MainWindow):
	def __init__(self):
		super().__init__()
		self.InfoText = ""
		self.XShift = self.InitialXShift
		self.YShift = self.InitialYShift
		self.ZShift = self.InitialZShift
		self.FocalLength = self.InitialFocalLength
		self.XAngle = self.InitialXAngle
		self.YAngle = self.InitialYAngle 
		self.ZAngle = self.InitialZAngle
		self.DisplayFunction = None
		self.CurrentFrame = None
		self.FrameMapper = Mapping3D.Mapping3D()
		self.PhotoAnalyzer = PhotoDivider.PhotoDivider()
		self.VideoStreamer = VideoProvider.VideoProvider(
		    os.path.join("Resources", "TrafficRecording.avi"), self.DisplayFrames
		)
		self.VideoStreamer.start()
		
	def DisplayFrames(self, FrameImage):
		DeviceFrame = self.GetPanelImage(FrameImage)
		self.DisplayFrame(self._SetLiveViewImage, DeviceFrame)
		MappingFrame = self.GetPanelImage(
		    self.FrameMapper.getImage(
		        FrameImage, self.XShift, self.YShift, self.ZShift, self.XAngle, self.YAngle, self.ZAngle, self.FocalLength
		    )
		)
		self.DisplayFrame(self._SetModelCalibrationImage, MappingFrame)
		PointCords = self.FrameMapper.getLineDirection()
		MotionMarkedFrame = self.GetPanelImage(self.PhotoAnalyzer.div_photos_proxy(FrameImage, *PointCords))
		self.DisplayFrame(self._SetDeterminigMotionAreaImage, MotionMarkedFrame)
		
		Distance, Speed = self.PhotoAnalyzer.speed(self.FrameMapper.pixelToDistance)
		if Distance > 200 or Speed > 70:
			self.ExecuteOnGUIThread(self.TurnOffLigths)
		else:
			self.ExecuteOnGUIThread(self.TurnOnLigths)
		self.DisplayInfoText("Distance: {} m\n\nVelocity: {} km/h".format(Distance, Speed))
		
	def DisplayFrame(self, DisplayFunction, DisplayFrame):
		self.DisplayFunction = DisplayFunction
		self.CurrentFrame = DisplayFrame
		self.ExecuteOnGUIThread(self.DisplayFrameProxy)		
		
	def DisplayFrameProxy(self):
		if self.CurrentFrame is not None:
			self.DisplayFunction(self.CurrentFrame)
			
	def DisplayInfoText(self, InfoText):
		self.InfoText = InfoText
		self.ExecuteOnGUIThread(self.DisplayInfoTextProxy)	
		
	def DisplayInfoTextProxy(self):
		self.SetInfoText(self.InfoText)		
		
	def XShiftChanged(self, Value):
		self.XShift = Value

	def YShiftChanged(self, Value):
		self.YShift = Value

	def ZShiftChanged(self, Value):
		self.ZShift = Value
	
	def FocalLengthChanged(self, Value):
		self.FocalLength = Value	

	def XAngleChanged(self, Value):
		self.XAngle = Value

	def YAngleChanged(self, Value):
		self.YAngle = Value

	def ZAngleChanged(self, Value):
		self.ZAngle = Value