from PyQt5 import QtWidgets

import MainInterface

if __name__ == "__main__":
	Application = QtWidgets.QApplication([])
	ApplicationInterface = MainInterface.MainInterface()
	ApplicationInterface.show()
	Application.exec_()