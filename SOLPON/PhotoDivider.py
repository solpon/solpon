import time
import numpy
import cv2

class PhotoDivider:
    def __init__(self):
        self.image1pix=None
        self.image2pix=None
        self.imagediff=None
        self.pix = (0,0)
        self.loaded_img=0
        self.move_array=[]
        self.speed_array=[]

    def div_photos_proxy(self, newImagePix, mov_x, mov_y):
        self.loaded_img+=1
        #if self.loaded_img==15:
        #self.speed(distance, time.time())
        if self.loaded_img==15:
            self.loaded_img = 0
            self.image1pix=self.image2pix
            self.image2pix=newImagePix
            #mov_x = 1
            #mov_y = -1
            if self.image1pix is not None:
                self.imagediff = self.div_photos(mov_x, mov_y)
                #cv2.line(self.imagediff, (0, 0), (700, 700), (255, 0, 0), 10)
                return self.imagediff
            else:
                return None
        else:
            return self.imagediff

    def speed(self, move): #distance, time
        time1 = time.time()
        move = move(self.pix)
        if len(self.move_array)>=3 and move>self.move_array[len(self.move_array)-1][0] and move>self.move_array[len(self.move_array)-2][0] and move>self.move_array[len(self.move_array)-3][0]:
            self.move_array.clear()
            self.speed_array.clear()
        self.move_array.append((move, time1))
        x=0
        while x<len(self.move_array)-1:
            distance=self.move_array[x+1][0]-self.move_array[x][0]
            time1=self.move_array[x+1][1]-self.move_array[x][1]
            self.speed_array.append(distance/time1)
            x+=1
        count=0


        check=10
        delete_count=7
        delete_array=[]
        for speed in self.speed_array:
            delete_array.append(0)
        #check=int(len(speed_array)*0.2)
        #delete_count=int(len(speed_array)*0.05)
        x=0
        y=0
        while x<len(self.speed_array):
            while y<check:
                if x >= 0 and x < len(self.speed_array ) and y >= 0 and y < len(self.speed_array) and abs(self.speed_array[x]-self.speed_array[y])>0.2*self.speed_array[x]:
                    delete_array[y]+=1
                y+=1
            x+=1
        x=0
        while x<len(delete_array):
            if(delete_array[x]>=delete_count):
                del self.speed_array[x]
            x+=1

        speed=0 if len(self.speed_array) == 0 else self.speed_array[0]
        x=1
        while x<len(self.speed_array):
            speed=(speed+self.speed_array[x])/2
            x+=1
        self.speed1=speed
        return move, speed

    def pix_abs_change(self, x, y):
        #if abs(self.image1pix[x, y][0] - self.image2pix[x, y][0]) > 15 and abs(self.image1pix[x, y][1] - self.image2pix[x, y][1]) > 15 and abs(self.image1pix[x, y][2] - self.image2pix[x, y][2]) > 15:
        x = int(x)
        y =int(y)
        if abs(self.image1pix[x, y][0] - self.image2pix[x, y][0]) > 25 and abs(self.image1pix[x, y][1] - self.image2pix[x, y][1]) > 25 and abs(self.image1pix[x, y][2] - self.image2pix[x, y][2]) > 25:
            return 1

    def div_photos(self, mov_x, mov_y):
        mov_x = int(mov_x)
        mov_y = int(mov_y)
        
        #p=time.time()
        #print(time.time()-p)

        #width=1366;
        #height=768;
        width=len(self.image1pix)
        height=len(self.image1pix[0])

        count=0
        mistake=0

        finish=0
        #p=time.time()
        imagepix = numpy.copy(self.image2pix)
        line=0
        if mov_x>0:
            for x in range(width, -1, -1):
                y=height-1
                while y>=0 and x>=0 and y<height and x<width:
                    if finish==1:
                        imagepix[x, y] = [0, 0, 0]
                    elif self.pix_abs_change(x, y)==1: #elif
                        #imagepix[x, y] = [0, 0, 0]
                        mistake=0
                        count += 1
                        if count>10:
                            finish=1
                            self.pix = x, y
                            if line==0:
                                #cv2.line(imagepix, (x, y), (x-500, y-500), (255, 0, 0), 10)
                                line=1
                            #return time.time()-p
                    else:
                        #imagepix[x, y] = [0, 0, 0]
                        mistake+=1
                        if mistake==5:
                            count=0
                            mistake=0
                    #print(x)
                    x+=mov_x
                    y+=mov_y

            for y in range(height-1, -1, -1):
                #for x in range(width-1, -1, mov_x):
                x=0
                while x>=0 and y>=0  and y<height and x<width:
                    if finish==1:
                        imagepix[x, y] = [0, 0, 0]
                    elif self.pix_abs_change(x, y)==1: #elif
                        #imagepix[x, y] = [0, 0, 0]
                        mistake = 0
                        count += 1
                        if count>10:
                            finish=1
                            self.pix = x, y
                            if line==0:
                                #cv2.line(imagepix, (x, y), (x-500, y-500), (255, 0, 0), 10)
                                line=1
                            #return time.time()-p
                    else:
                        #imagepix[x, y] = [0, 0, 0]
                        mistake += 1
                        if mistake == 10:
                            count = 0
                            mistake=0
                    #print(x)
                    x+=mov_x
                    y+=mov_y

        if mov_x<=0:
            for x in range(0, width, 1):
                y=height-1
                while y>=0 and x>=0 and y<height and x<width:
                    if finish==1:
                        imagepix[x, y] = [0, 0, 0]
                    elif self.pix_abs_change(x, y)==1: #elif
                        #imagepix[x, y] = [0, 0, 0]
                        mistake = 0
                        count += 1
                        if count>20:
                            finish=1
                            self.pix = x, y
                            if line==0:
                                #cv2.line(imagepix, (0, 0), (700, 700), (255, 0, 0), 10)
                                line=1
                            #return time.time()-p
                    else:
                        #imagepix[x, y] = [0, 0, 0]
                        mistake += 1
                        if mistake == 5:
                            count = 0
                    #print(x)
                    x+=mov_x
                    y+=mov_y


            for y in range(height-1, -1, -1):
                #for x in range(width-1, -1, mov_x):
                x=width-1
                while x>=0 and y>=0  and y<height and x<width:
                    if finish==1:
                        imagepix[x, y] = [0, 0, 0]
                    elif self.pix_abs_change(x, y)==1: #elif
                        #imagepix[x, y] = [0, 0, 0]
                        mistake = 0
                        count += 1
                        if count>20:
                            finish=1
                            self.pix = x, y
                            #cv2.line(imagepix, (0, 0), (700, 700), (255, 0, 0), 10)
                            #return time.time()-p
                    else:
                        #imagepix[x, y] = [0, 0, 0]
                        mistake += 1
                        if mistake == 5:
                            count = 0
                    #print(x)
                    x+=mov_x
                    y+=mov_y

        #print(time.time()-p)

        return imagepix
        #image2.save('Test1.png')

def main():
    '''import cv2
    image1pix= cv2.imread('A.bmp')
    image2pix = cv2.imread('B.bmp')
    photoDivider=PhotoDivider()
    photoDivider.div_photos_proxy(image1pix)
    image=photoDivider.div_photos_proxy(image2pix)
    #for i in range():
    #    print(photoDivider.div_photos_proxy(image2pix))
    #    print(photoDivider.loaded_img)
    cv2.imwrite('out.bmp',image)'''
    from PIL import Image

    image1 = Image.open('A.png')
    image1pix = image1.load()
    image2 = Image.open('B.png')
    image2pix = image2.load()
    photoDivider = PhotoDivider()
    photoDivider.div_photos_proxy(image1pix)
    image2pix = photoDivider.div_photos_proxy(image2pix)
    image2.save('Test2.png')


if __name__ == '__main__':
    main()
