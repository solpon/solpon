import math

import cv2


class Mapping3D:
    cameraLocation = None
    xAngle = None
    yAngle = None
    zAngle = None
    focal = None
    width = None
    height = None
    projectionMatrixP = None

    @staticmethod
    def detMatrix3x3(matrix):
        assert len(matrix) == 3
        assert len(matrix[0]) == 3
        sum = 0.0
        for i in range(3):
            sum += matrix[i][0] * matrix[(i + 1) % 3][1] * matrix[(i + 2) % 3][2]
            sum -= matrix[i][2] * matrix[(i + 1) % 3][1] * matrix[(i + 2) % 3][0]
        return sum

    @staticmethod
    def sgn(x):
        if x < 0:
            return -1
        elif x > 0:
            return 1
        else:
            return 0

    @staticmethod
    def multiplyMatrixes(matrix1, matrix2):
        assert len(matrix1[0]) == len(matrix2)
        matrix3 = []
        for i in range(len(matrix1)):
            matrix3.append([])
            for j in range(len(matrix2[0])):
                matrix3[i].append(0)
                for k in range(len(matrix1[0])):
                    matrix3[i][j] += matrix1[i][k] * matrix2[k][j]
        return matrix3

    @staticmethod
    def addMatrixes(matrix1, matrix2):
        assert len(matrix1) == len(matrix2)
        assert len(matrix1[0]) == len(matrix2[0])
        matrix3 = []
        for i in range(len(matrix1)):
            matrix3.append([])
            for j in range(len(matrix1[0])):
                matrix3[i].append(matrix1[i][j] + matrix2[i][j])
        return matrix3

    def calcPoint(self, point3d):
        fx = fy = self.focal
        cx = self.width / 2
        cy = self.height / 2
        self.projectionMatrixP = [
            [fx, 0.0, cx],
            [0.0, fy, cy],
            [0.0, 0.0, 1.0]
        ]
        return self.point3dTo2d(point3d)

    def pixelToDistance(self, point2d):
        minValue = -100
        maxValue = 100
        midX = (maxValue + minValue) / 2
        while maxValue - minValue > 0.1:
            point3dLeft = [
                [midX],
                [0],
                [0]
            ]
            point3dRight = [
                [midX],
                [100],
                [0]
            ]
            xLeft, yLeft = self.point3dTo2d(point3dLeft)
            xRight, yRight = self.point3dTo2d(point3dRight)
            matrix = [
                [xLeft, yLeft, 1],
                [xRight, yRight, 1],
                [point2d[0], point2d[1], 1]
            ]
            sgn = self.sgn(self.detMatrix3x3(matrix))
            if sgn < 0:
                minValue = midX
            elif sgn > 0:
                maxValue = midX
            else:
                return midX
            midX = (maxValue + minValue) / 2
        return midX

    def getLineDirection(self):
        distanceZ = 50
        point3dLeft = [
            [0],
            [0],
            [distanceZ]
        ]
        point3dRight = [
            [30],
            [0],
            [distanceZ]
        ]
        pointA = self.point3dTo2d(point3dLeft)
        pointB = self.point3dTo2d(point3dRight)
        std = min(abs(pointA[0] - pointB[0]), abs(pointA[1] - pointB[1]))
        if std == 0:
            std = 0.001
        x = (pointA[0] - pointB[0]) / std
        y = abs(pointA[1] - pointB[1]) / std
        return x, -y

    def point3dTo2d(self, point3d):
        point3d = self.multiplyMatrixes(self.Rx(self.xAngle), point3d)
        point3d = self.multiplyMatrixes(self.Ry(self.yAngle), point3d)
        point3d = self.multiplyMatrixes(self.Rz(self.zAngle), point3d)
        point3d = self.addMatrixes(self.cameraLocation, point3d)
        point2d = self.multiplyMatrixes(self.projectionMatrixP, point3d)
        x = point2d[0][0]
        y = point2d[1][0]
        if point2d[2][0] == 0:
            d = 0.0001
        else:
            d = point2d[2][0]
        return int(x // d), int(y // d)

    @staticmethod
    def Rx(angle):
        return [
            [1.0, 0.0, 0.0],
            [0.0, math.cos(angle), -math.sin(angle)],
            [0.0, math.sin(angle), math.cos(angle)]
        ]

    @staticmethod
    def Ry(angle):
        return [
            [math.cos(angle), 0.0, math.sin(angle)],
            [0.0, 1.0, 0.0],
            [-math.sin(angle), 0.0, math.cos(angle)]
        ]

    @staticmethod
    def Rz(angle):
        return [
            [math.cos(angle), -math.sin(angle), 0.0],
            [math.sin(angle), math.cos(angle), 0.0],
            [0.0, 0.0, 1.0]
        ]

    def printLines(self, baseImage, lines):
        xd = 0
        for line in lines:
            if (line[0], line[1]) != (line[2], line[3]):
                if line[0] < self.width and line[1] < self.height and line[2] < self.width and line[3] < self.height:
                    if line[0] > 0 and line[1] > 0 and line[2] > 0 and line[3] > 0:
                        cv2.line(baseImage, (line[0], line[1]), (line[2], line[3]),
                                 ((xd % 3) * 50, ((xd + 1) % 3) * 50, ((xd + 2) % 3) * 50), 2)
            xd += 1
        return baseImage

    def getImage(self, baseImage, x, y, z, xAngle, yAngle, zAngle, focal):
        self.focal = focal
        self.width = len(baseImage[0])
        self.height = len(baseImage)
        self.xAngle = xAngle / 180.0 * math.pi
        self.yAngle = yAngle / 180.0 * math.pi
        self.zAngle = zAngle / 180.0 * math.pi
        self.cameraLocation = [[x], [y], [z]]
        pointStart = self.calcPoint([[0], [0], [0]])
        pointX = self.calcPoint([[100], [0], [0]])
        pointY = self.calcPoint([[0], [5], [0]])
        pointZ = self.calcPoint([[0], [0], [100]])
        lines = [
            [pointStart[0], pointStart[1], pointX[0], pointX[1]],
            [pointStart[0], pointStart[1], pointY[0], pointY[1]],
            [pointStart[0], pointStart[1], pointZ[0], pointZ[1]],
        ]
        for i in range(5):
            point1 = self.calcPoint([[0], [0], [20 * i]])
            point2 = self.calcPoint([[50], [0], [20 * i]])
            lines.append([point1[0], point1[1], point2[0], point2[1]])
        return self.printLines(baseImage, lines)
