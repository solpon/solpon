import cv2
import threading

class VideoProvider(threading.Thread):
	def __init__(self, VideoSource, ShowFrameCallback):
		super().__init__()
		self.VideoSource = cv2.VideoCapture(VideoSource) if VideoSource else cv2.VideoCapture(0)
		self.ShowFrame = ShowFrameCallback

	def run(self):
		while True:
			_, ImageFrame = self.VideoSource.read()
			self.ShowFrame(ImageFrame)
